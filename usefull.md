#For review
we use Laravel 7.16.1

#beanstalkd

https://beanstalkd.github.io/
exists a lot of libraries for wtih beanstalkd from php
```shell script
docker-compose run --rm composer require pda/pheanstalk
```
`hostname = 'beanstalk' ` in `config/queue.php` or use
`'host' =>env('BEANSTALK_HOST', 'localhost')`, in `config/queue.php`
.env 
QUEUE_CONNECTION=beanstalkd
.env.example
see additional info from Yevhen Turovets
https://www.youtube.com/watch?v=IsS8HO4knBY&list=PLoonZ8wII66iYtCIjSytFjqysPDOjrJVn&index=1

## work with queue
###Rub queue
```shell script
docker-compose exec app 
php artisan queue:work beanstalkd
--tries=1
--queue=notifications, email
--timeout=30
--sleep=30
```

```docker-compose exec app php artisan queue:work beanstalkd --tries=1
```

###stope the queue when it is empty
```shell script
docker-compose exec app php artisan queue:work --stop-when-empty
```
###wait for finishing the current job and restarts the queue
```shell script
docker-compose exec app php artisan queue:restart
```
##creation of Jobs
```shell script
docker-compose exec app php artisan make:job MyJob
```

#Miscellaneous
Command for working
```shell script
docker-compose run --rm composer require ... # установка composer зависимостей
docker-compose run --rm frontend npm install ... # установка npm зависимостей
docker-compose logs -f websocket ... # лог веб-сокет сервера
```
How we can find version of Laravel
```shell script
docker-compose exec app php artisan -V
```
```shell script
docker-compose exec app php artisan queue:work beanstalkd --tries=1

```
Laravel has cache and 
```shell script
 docker-compose exec app php artisan cache:clear
```
