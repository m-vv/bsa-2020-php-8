<?php

namespace App\Jobs;

use App\Actions\Image\ApplyFilterResponse;
use App\Entities\User;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\ImageApiService;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected Image $image;
    protected string $filter;
    protected User $user;

    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Image  $image
     * @param  string  $filter
     */
    public function __construct(User $user, Image $image, string $filter)
    {
        $this->image = $image;
        $this->filter = $filter;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param  ImageApiService  $imageService
     * @return void
     */
    public function handle(ImageApiService $imageService)
    {
        $result = $imageService->applyFilter($this->image->getSrc(), $this->filter);
        $this->user->notify(
            new ImageProcessedNotification(
                new Image($this->image->getId(), $result)
            )
        );
    }
    public function failed(\Exception $e)
    {
        $this->user->notify(
            new ImageProcessingFailedNotification(
                new Image($this->image->getId(), $this->image->getSrc()),
                $this->user,
                $this->filter,
                $e->getMessage(),
            )
        );
    }
}
