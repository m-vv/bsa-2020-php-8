<?php

namespace App\Notifications;

use App\Entities\User;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImageProcessingFailedNotification extends Notification
{
    use Queueable;

    protected Image $image;
    protected User $user;
    protected string $filter;
    protected string $exceptionMessage;


    /**
     * Create a new notification instance.
     *
     * @param  Image  $image
     * @param  User  $user
     * @param  string  $filter
     * @param  string  $exceptionMessage
     */
    public function __construct(Image $image, User $user, string $filter, string $exceptionMessage)
    {
        $this->image = $image;
        $this->user = $user;
        $this->filter = $filter;
        $this->exceptionMessage = $exceptionMessage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->view(
                'email',
                [
                    'user' => $this->user,
                    'filter' => $this->filter,
                    'image' => $this->image,
                ]
            );
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'status'=> 'failed',
                'message' =>$this->exceptionMessage,
                'image' =>
                    [
                        'id'=> $this->image->getId(),
                        'src'=>$this->image->getSrc(),
                     ],
            ]
        );
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
