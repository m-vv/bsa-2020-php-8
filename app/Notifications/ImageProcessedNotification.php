<?php

namespace App\Notifications;

use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    protected Image $image;

    /**
     * Create a new notification instance.
     *
     * @param  Image  $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
               'status' =>  'success',
                'image'  =>
                    [
                       'id'=> $this->image->getId(),
                       'src'=>$this->image->getSrc(). ' updated',
                    ]
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
