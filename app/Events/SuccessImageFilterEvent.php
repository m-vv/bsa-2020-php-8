<?php

namespace App\Events;

use App\Actions\Image\ApplyFilterResponse;
use App\Values\Image;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SuccessImageFilterEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    private Image $image;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('image-job-channel');
    }

    public function broadcastWith()
    {
        //$result = new ApplyFilterResponse($this->image);
        return ['id' => 20];
    }
}
