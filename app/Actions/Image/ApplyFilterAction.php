<?php

declare(strict_types=1);

namespace App\Actions\Image;

use App\Jobs\ImageJob;
use App\Services\Contracts\ImageApiService;
use App\Values\Image;
use Illuminate\Support\Facades\Auth;

class ApplyFilterAction
{
    private ImageApiService $imageApiService;

    public function __construct(ImageApiService $imageApiService)
    {
        $this->imageApiService = $imageApiService;
    }

    public function execute(Image $image, string $filter)
    {
        $user = Auth::user();
        ImageJob::dispatch($user, $image, $filter)->allOnConnection('beanstalkd');
    }
}
